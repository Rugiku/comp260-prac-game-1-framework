﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Move : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	public Vector2 move;
	public Vector2 velocity;
	public float maxSpeed = 5.0f;

	void Update() {
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal2");
		direction.y = Input.GetAxis ("Vertical2");

		Vector2 velocity = direction * maxSpeed;

		transform.Translate (velocity * Time.deltaTime);

		Vector2 move = velocity * Time.deltaTime;
		// move the object
		transform.Translate(move);
	}

}
