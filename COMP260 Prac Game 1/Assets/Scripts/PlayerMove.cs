﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	public Vector2 move1;
	public Vector2 velocity1;
	public string horizontalAxis;
	public string verticalAxis;

	public float maxSpeed = 5.0f;

	void Update() {
		//Player 1
		Vector2 direction1;
		direction1.x = Input.GetAxis (horizontalAxis);
		direction1.y = Input.GetAxis (verticalAxis);


		Vector2 velocity1 = direction1 * maxSpeed;


		transform.Translate (velocity1 * Time.deltaTime);


		Vector2 move1 = velocity1 * Time.deltaTime;

		// move the object
		transform.Translate(move1);




	}

}
