﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	public float speed = 6.0f;
	public float turnSpeed = 180.0f;
	public Transform target1;
	public Transform target2;
	public Vector2 heading = Vector3.right; 



	void Update() {
		// get the vector from the bee to the target 
		Vector2 direction1 = target1.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;
		Vector2 direction = target2.position - transform.position;

		//measure the vector between each player and the closest becomes the target
//		if (PlayerMove(direction.magnitude) > Player2Move(direction.magnitude)){
//			target = PlayerMove;
//		} else {
//			target = Player2Move;
//		}

		if (Vector2.Distance(target1.position, transform.position) < Vector2.Distance(target2.position, transform.position)){
			direction = target1.position - transform.position;
		}

//		if (direction1.magnitude > direction2.magnitude) {
//			finalDirection = direction2;
//		} else {
//			finalDirection = direction1;
//		}


			
		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft (heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate (angle);
		} else {
			// target on right, rotate clockwise
			heading = heading.Rotate (-angle);
		}

		transform.Translate (heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction1 = target1.position - transform.position;
		Gizmos.DrawRay(transform.position, direction1);

		Gizmos.color = Color.yellow;
		Vector2 direction2 = target2.position - transform.position;
		Gizmos.DrawRay (transform.position, direction2);

	}



}
